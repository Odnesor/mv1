/**
 * Main validation form
 * @param {form} jquery obj - Form
 * @param {options} obj - object width params
 */

$.validator.setDefaults({
    ignore: []
});

const only_num = /^[0-9]+$/;
const nums_space = /^[0-9\s]+$/;
const nums_defis = /^[0-9\-]+$/;
const nums_space_defis = /^[0-9\-\s]+$/;

const cyrillic = /^[аА-яЯіІєЄїЇґҐ]+$/i;
const cyrillic_space = /^[аА-яЯіІєЄїЇґҐ\s]+$/i;
const cyrillic_defis = /^[аА-яЯіІєЄїЇґҐ\-]+$/i;
const cyrillic_defis_space = /^[аА-яЯіІєЄїЇґҐ\-\s]+$/i;
const cyrillic_apostrof = /^[аА-яЯіІєЄїЇґҐ\'\`]+$/i;
const cyrillic_apostrof_space = /^[аА-яЯіІєЄїЇґҐ\'\`\s]+$/i;
const cyrillic_apostrof_defis = /^[аА-яЯіІєЄїЇґҐ\'\`\-]+$/i;
const cyrillic_apostrof_defis_space = /^[аА-яЯіІєЄїЇґҐ\'\`\-\s]+$/i;

const cyrillic_nums = /^[аА-яЯіІєЄїЇґҐ0-9]+$/i;
const cyrillic_nums_space = /^[аА-яЯіІєЄїЇґҐ0-9\s]+$/i;
const cyrillic_nums_defis = /^[аА-яЯіІєЄїЇґҐ0-9\-]+$/i;
const cyrillic_nums_defis_space = /^[аА-яЯіІєЄїЇґҐ0-9\-\s]+$/i;
const cyrillic_nums_apostrof = /^[аА-яЯіІєЄїЇґҐ0-9\'\`]+$/i;
const cyrillic_nums_apostrof_space = /^[аА-яЯіІєЄїЇґҐ0-9\'\`\s]+$/i;
const cyrillic_nums_apostrof_defis = /^[аА-яЯіІєЄїЇґҐ0-9\'\`\-]+$/i;
const cyrillic_nums_apostrof_defis_space = /^[аА-яЯіІєЄїЇґҐ0-9\'\`\-\s]+$/i;


const nums_letters = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9]*$/;
const nums_letters_space = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9\s]*$/;
const nums_letters_special = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9\s\'\`\"\-\,\.]*$/;

const email_reg = /^(([^<>()\[\]\\.,;:\s@"]{2,62}(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z-аА-яЯ\-0-9]+\.)+[a-zA-Z-аА-яЯ]{2,62}))$/;

const mobile_regex = /^[+]*[0-9]{1,2}[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\.\/0-9]*$/g;
const mobile_regex2 = /^[0-9\(\)\+\s\-]+$/;
const mobile_mask = '+7(\\999)999-99-99';
const mobile_mask_option = {placeholder: "+7(9XX)XXX-XX-XX", autoclear: false};

const inputs = {
    'first_name': {
        'rules': {
            regex: cyrillic_space,
            maxlength: 20,
            messages: {
                regex: message.cyrillic,
                maxlength: message.max_length + 20
            }
        }
    },
    'middle_name': {
        'rules': {
            regex: cyrillic_space,
            maxlength: 20,
            messages: {
                regex: message.cyrillic,
                maxlength: message.max_length + 20
            }
        }
    },
    'last_name': {
        'rules': {
            regex: cyrillic_space,
            maxlength: 20,
            messages: {
                regex: message.cyrillic,
                maxlength: message.max_length + 20
            }
        }
    },

    'email': {
        'rules': {
            regex: email_reg,
            maxlength: 320,
            messages: {
                regex: message.email,
                maxlength: message.max_length + 320
            }
        }
    },

    'birth_date': {
        'rules': {
            check_date_of_birth: true
        }
    },
    'passport_date': {
        'rules': {
            check_date_of_passport: true
        }
    },

    'city': {
        'rules': {
            regex: cyrillic_defis_space,
            maxlength: 30,
            messages: {
                regex: message.cyrillic_defis,
                maxlength: message.max_length + 30
            }
        }
    },

    'passport_series': {
        'rules': {
            regex: nums_space_defis,
            messages: {
                regex: message.not_enough
            }
        },
        'mask': '99 99',
        'mask_option': {placeholder: "XX XX", autoclear: false},
    },
    'passport_num': {
        'rules': {
            regex: nums_space_defis,
            messages: {
                regex: message.not_enough
            }
        },
        'mask': '999999',
        'mask_option': {placeholder: "XXXXXX", autoclear: false},
    },
    'passport_code': {
        'rules': {
            regex: nums_space_defis,
            messages: {
                regex: message.not_enough
            }
        },
        'mask': '999-999',
        'mask_option': {placeholder: "XXX-XXX", autoclear: false},
    },



    'only_num': {
        'rules': {
            regex: only_num,
            messages: {
                regex: message.only_num
            }
        }
    },
    'nums_space_defis': {
        'rules': {
            regex: nums_space_defis,
            messages: {
                regex: message.nums_space_defis
            }
        }
    },



    'cyrillic': {
        'rules': {
            regex: cyrillic,
            messages: {
                regex: message.cyrillic
            }
        }
    },
    'cyrillic_space': {
        'rules': {
            regex: cyrillic_space,
            messages: {
                regex: message.cyrillic
            }
        }
    },
    'cyrillic_defis': {
        'rules': {
            regex: cyrillic_defis,
            messages: {
                regex: message.cyrillic_defis
            }
        }
    },
    'cyrillic_defis_space': {
        'rules': {
            regex: cyrillic_defis_space,
            messages: {
                regex: message.cyrillic_defis
            }
        }
    },
    'cyrillic_apostrof_defis': {
        'rules': {
            regex: cyrillic_apostrof_defis,
            messages: {
                regex: message.cyrillic_apostrof_defis
            }
        }
    },
    'cyrillic_apostrof_defis_space': {
        'rules': {
            regex: cyrillic_apostrof_defis_space,
            messages: {
                regex: message.cyrillic_apostrof_defis
            }
        }
    },




    'cyrillic_nums': {
        'rules': {
            regex: cyrillic_nums,
            messages: {
                regex: message.cyrillic_nums
            }
        }
    },
    'cyrillic_nums_space': {
        'rules': {
            regex: cyrillic_nums_space,
            messages: {
                regex: message.cyrillic_nums
            }
        }
    },
    'cyrillic_nums_defis': {
        'rules': {
            regex: cyrillic_nums_defis,
            messages: {
                regex: message.cyrillic_nums_defis
            }
        }
    },
    'cyrillic_nums_defis_space': {
        'rules': {
            regex: cyrillic_nums_defis_space,
            messages: {
                regex: message.cyrillic_nums_defis
            }
        }
    },
    'cyrillic_nums_apostrof_defis': {
        'rules': {
            regex: cyrillic_nums_apostrof_defis,
            messages: {
                regex: message.cyrillic_nums_apostrof_defis
            }
        }
    },
    'cyrillic_nums_apostrof_defis_space': {
        'rules': {
            regex: cyrillic_nums_apostrof_defis_space,
            messages: {
                regex: message.cyrillic_nums_apostrof_defis
            }
        }
    },


    'nums_letters': {
        'rules': {
            regex: nums_letters,
            messages: {
                regex: message.nums_letters
            }
        }
    },
    'nums_letters_space': {
        'rules': {
            regex: nums_letters_space,
            messages: {
                regex: message.nums_letters_space
            }
        }
    },
    'nums_letters_special': {
        'rules': {
            regex: nums_letters_special,
            messages: {
                regex: message.nums_letters_special
            }
        }
    },

}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

const responsiveStyles = {
    991: {
        'html': {
            'font-size': '9px'
        }
    },
    768: {
        'html': {
            'font-size': '8px'
        }
    },
    666: {
        'html': {
            'font-size': '10px'
        },
        '.forms__block__rows__item': {
            'width': 'calc(100% - 3.2rem)'
        },
        '.forms__block__rows__item:nth-last-child(-n + 3):not(:last-child)': {
            'margin-bottom': '4rem'
        },
        '.forms__block__rows__item > *': {
            'margin-bottom': '0 !important'
        },
        '.forms__slides': {
            'flex-direction': 'column',
            'margin': '0!important'
        },
        '.forms__slides__slide': {
            'margin': '0 0 3rem',
            'max-width': 'unset'
        }
    }
}


/* regionstart
         * слушаем событие изменения размеров документа
         * посылаем запрос в родительское окно,
         * для изменения высоты iframe, в котором находится наша форма
         * */
var change_height = function(){
    var po = getParameterByName("po"),
        obj = null,
        windowWidth = $(window).width();

    $.each(responsiveStyles, function (resWidth, resStyles){
        if(windowWidth<=resWidth){
            $.each(resStyles, function (thisElement, thisStyles) {
                $(thisElement).css(thisStyles)
            })
        } else {
            $.each(resStyles, function (thisElement, _) {
                $(thisElement).removeAttr('style')
            })
        }
    })
    if(po){
        obj = {
            name: "change_height",
            value: $("html").outerHeight( true )
        };
        try {
            parent.postMessage(JSON.stringify(obj), po);
        }catch(e){}
    }
    obj = null;
};
/* regionend */

$(window).on("load", change_height);
$(document).ready(change_height);
$(window).on("resize", change_height);

function validatorMethods() {
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || cyrillic_apostrof.test(value);
    }, message.lettersonly);


    $.validator.addMethod("check_date_of_birth", function (value, element) {
        let container = $(element).closest('.form-input__date');
        let full_date = [];
        let can_continue = true;

        $(container.find('input[type=hidden]').get().reverse()).each(function() {
            let this_value = $(this).attr('data-value');
            if(typeof this_value === 'undefined')
                this_value = $(this).val();
            full_date.push(this_value);
            if(typeof this_value === 'undefined')
                can_continue=false;
        });

        if(!can_continue){
            return true;
        }

        var mydate = new Date();
        mydate.setFullYear(full_date[0], full_date[1] - 1, full_date[2]);

        var minDate = new Date();
        minDate.setYear(minDate.getFullYear() - 18);

        // var maxDate = new Date();
        // maxDate.setYear(maxDate.getFullYear() - 66);

        if (minDate < mydate) {
            return false;
        }


        // if (maxDate > mydate) {
        //     setTimeout(function () {
        //         container.closest('.form-input').find('.form-input__error').html('<label class="errorText">'+message.max_age+'</label>')
        //     }, 200)
        //
        //     return false;
        // }


        return true;
    }, message.min_age);

    $.validator.addMethod("check_date_of_passport", function (value, element) {
        let birth_container = $('input[data-validation="birth_date"]').closest('.form-input__date');
        let birth_date_arr = [];
        let can_continue = true;

        $(birth_container.find('input[type=hidden]').get().reverse()).each(function() {
            let this_value = $(this).attr('data-value');
            if(typeof this_value === 'undefined')
                this_value = $(this).val();

            birth_date_arr.push(this_value);

            if(typeof this_value === 'undefined')
                can_continue=false;
        });

        if(!can_continue){
            return true;
        }

        let birth_date = new Date();
        birth_date.setFullYear(birth_date_arr[0], birth_date_arr[1] - 1, birth_date_arr[2]);
        birth_date.setYear(birth_date.getFullYear() + 13);



        let pass_container = $(element).closest('.form-input__date');
        let pass_date_arr = [];

        $(pass_container.find('input[type=hidden]').get().reverse()).each(function() {
            let this_value = $(this).attr('data-value');
            if(typeof this_value === 'undefined')
                this_value = $(this).val();
            pass_date_arr.push(this_value);
            if(typeof this_value === 'undefined')
                can_continue=false;
        });
        if(!can_continue){
            return true;
        }
        let pass_date = new Date();
        pass_date.setFullYear(pass_date_arr[0], pass_date_arr[1] - 1, pass_date_arr[2]);


        if(birth_date >= pass_date) {
            return false;
        }

        return true;
    }, message.passport_date);


    $.validator.addMethod("regex", function (value, element, regexp) {
        var re = new RegExp(regexp);

        return value == '' || re.test(value);
    });


}

function inputsRules($form) {
    $('[required]', $form).each(function () {
        $(this).rules("add", {
            required: true,
            messages: {
                required: message.empty
            }
        });
    });

    $('[maxlength]', $form).each(function () {
        let max = $(this).attr('maxlength');

        if(!max) return false;
        $(this).rules("add", {
            maxlength: max,
            messages: {
                maxlength: message.max_length + max
            }
        });
    });

    $('[minlength]', $form).each(function () {
        let min = $(this).attr('minlength');
        $(this).rules("add", {
            minlength: min,
            messages: {
                minlength: message.min_length + min
            }
        });
    });

    if ($('[data-validation=tel]', $form).length) {
        $('[data-validation=tel]', $form).each(function () {
            let this_mask_options = mobile_mask_option;
            if(this_mask_options.placeholder !== $(this).attr('placeholder'))
                this_mask_options.placeholder = $(this).attr('placeholder');

            $(this).inputmask(mobile_mask, this_mask_options);
        })

        $('[data-validation=tel]', $form).rules("add", {
            regex: mobile_regex2,
            messages: {
                regex: message.not_enough,
                required: message.empty
            }
        });
    }

    $.each(inputs, function (data_id, opts) {
        var $input = $('[data-validation='+data_id+']', $form);

        if($input.length) {
            if (typeof opts.rules !== 'undefined') {
                validationRule($input,'add', opts.rules)
            }
            if (typeof opts.mask !== 'undefined' && typeof opts.mask_option !== 'undefined'){
                let mask_options = opts.mask_option;
                if($input.attr('placeholder') !== opts.mask_option.placeholder) {
                    mask_options.placeholder = $input.attr('placeholder');
                }

                $input.inputmask(opts.mask, mask_options)
            } else if(typeof opts.mask !== 'undefined'){
                $input.inputmask(opts.mask)
            }
        }
    });
}

function validate(form, options) {
    var setings = {
        errorFunction: null,
        submitFunction: ajaxSubmit,
        highlightFunction: null,
        unhighlightFunction: null
    };

    validatorMethods();

    $.extend(setings, options);

    var $form = $(form);

    if ($form.length && $form.attr('novalidate') === undefined) {
        $form.on('submit', function (e) {
            e.preventDefault();
        });

        $form.validate({
            errorClass: 'errorText',
            focusCleanup: false,
            onclick: false,
            onfocusout: false,
            // onkeyup: false,
            focusInvalid: true,
            invalidHandler: function (event, validator) {
                if (typeof setings.errorFunction === 'function') {
                    setings.errorFunction(form);
                }
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.closest('.form-input').find('.form-input__error'));
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('error');
                $(element).closest('.form-input').addClass('error');
                if (typeof setings.highlightFunction === 'function') {
                    setings.highlightFunction(form);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('error');
                $(element).closest('.form-input').removeClass('error');
                if (typeof setings.unhighlightFunction === 'function') {
                    setings.unhighlightFunction(form);
                }
            },
            submitHandler: function (form) {
                $('[type=submit]').each(function (){
                    $(this).attr('disabled', 'disabled');
                });
                if (typeof setings.submitFunction === 'function') {
                    setings.submitFunction(form);
                } else {
                    $form[0].ajaxSubmit();
                }
            }
        });

        inputsRules($form);
    }
}

function validationRule(input, action, rule){
    input.each(function () {
        if(rule == 'required' && action == 'add') {
            $(this).rules('add', {
                required: true,
                messages: {
                    required: message.empty
                }
            });
        } else if(rule == 'required') {
            $(this).removeAttr('required')
            $(this).rules(action, rule);
        } else {
            $(this).rules(action, rule);
        }
    })
}



function inputDropdown() {
    let dropdowns = $('.form-input__dropdown');
    let input_container = dropdowns.parent();
    let input = input_container.find('input[type=text]');
    let dropdown = input_container.find('.form-input__dropdown__container');
    let dropdown_item = dropdown.find('li');
    let label = dropdowns.closest('.form-input').find('label')

    input.on('click', function () {
        let this_container = $(this).closest('.form-input__dropdown');
        let this_dropdown = this_container.find('.form-input__dropdown__container');


        dropdown.not(this_dropdown).slideUp(100);
        dropdowns.not(this.closest('.form-input__dropdown')).removeClass('open');

        this_dropdown.slideToggle(300);
        this_container.toggleClass('open');


        if(dropdowns.hasClass('open')) {
            label.addClass('open')
        } else {
            label.removeClass('open')
        }
    });

    dropdown.on('click', function () {
        $(this).slideUp(300);
        dropdowns.removeClass('open');
        label.removeClass('open');
    });

    inputDropdownListEvents($('.form-input__dropdown'));


    $(document).on('click', function (e) {
        let target = $(e.target);

        if(!target.closest('.form-input__date__input').length){
            dropdown.slideUp(300);
            dropdowns.removeClass('open');
            label.removeClass('open');
        }
    });
}

function inputDropdownListEvents(dropdowns) {
    let input_container = dropdowns.parent();
    let dropdown = input_container.find('.form-input__dropdown__container');
    let dropdown_item = dropdown.find('li');

    dropdown_item.on('click', function () {
        let this_item = $(this);
        let this_text = this_item.text();
        let this_container = this_item.closest('.form-input__dropdown').parent();
        let this_main_input = this_container.find('input[type=text]');
        let this_hidden_input = this_container.find('input[type=hidden]');
        let data_value = this_item.data('value');
        let all_items = this_container.find('li')

        all_items.removeClass('selected')
        this_item.addClass('selected');

        if(data_value){
            this_main_input.attr('data-value', data_value)
            this_hidden_input.attr('data-value', data_value)
            this_hidden_input.val(data_value)
        } else {
            this_hidden_input.val(this_text)
        }

        this_main_input.val(this_text)
        this_hidden_input.trigger('change')
        this_hidden_input.valid()
    });
}

function inputDate(){
    let dates = $('.form-input__date');

    function reloadDropdowns(this_date) {
        let this_container = this_date;
        let input_containers = this_container.find('.form-input__date__input');
        let this_days = this_container.find('.day_input');
        let days_list = this_days.closest('.form-input__dropdown').find('.form-input__dropdown__list');
        let this_months = this_container.find('.month_input');
        let months_list = this_months.closest('.form-input__dropdown').find('.form-input__dropdown__list');
        let this_years = this_container.find('.year_input');
        let year_list = this_years.closest('.form-input__dropdown').find('.form-input__dropdown__list');

        let num_month = parseInt(this_months.closest('.form-input__date__input').find('input[type=hidden]').attr('data-value'));

        let today = new Date();
        let curr_year = today.getFullYear();

        let max_days = 31;
        let min_year = 1920;

        if (num_month && this_years.val()) {
            max_days = new Date(parseInt(this_years.val()), num_month, 0).getDate();
        }

        // if(){
        //     curr_year = 2000
        // }

        days_list.empty();
        for (let days = 1; days <= max_days; days++) {
            days_list.append('<li data-value="' + days + '">' + days + '</li>')
        }

        months_list.empty();
        $.each(months_array, function (index, month) {
            months_list.append('<li data-value="' + (index + 1) + '">' + month + '</li>')
        })

        year_list.empty();
        for (min_year; min_year <= curr_year; min_year++) {
            year_list.append('<li data-value="' + min_year + '">' + min_year + '</li>')
        }

        input_containers.each(function () {
            let hidden_inp = $(this).find('input[type=hidden]');
            let dropd_inp = $(this).find('.form-input__dropdown>input');
            let selected = hidden_inp.attr('data-value');

            if(selected) {
                let item = $(this).find('li[data-value=' + selected + ']')

                if (item.length) {
                    item.addClass('selected')
                } else {
                    hidden_inp.val('')
                    hidden_inp.attr('data-value', '')
                    dropd_inp.val('')
                    dropd_inp.attr('data-value', '')
                }
            }
        });

    }

    dates.each(function () {
        reloadDropdowns($(this))

        $(this).find('input[type=hidden]').on('change', function () {
            reloadDropdowns($(this).closest('.form-input__date'));
            inputDropdownListEvents($(this).closest('.form-input__date'))
        })
    })
    inputDropdownListEvents($('.form-input__dropdown'))
}

function ajaxSubmit(form){
    let apiUrl = $(form).find('input[name="api_form_handler"]').prop("defaultValue");
    let formData = new FormData(form);
    $.ajax({
        url: apiUrl,
        method: 'POST',
        data: formData,
        processData: false,  // Сообщить jQuery не передавать эти данные
        contentType: false,
        success: function (response) {
            let submitBtn = $(form).find('button[type=submit]').closest('.forms__block');
            let successBlock = $(form).find('.forms__success');

            submitBtn.fadeOut(0);
            successBlock.css("display", "flex").hide().fadeIn(300);
        },
        error: function () {
            $('button[type=submit]').removeAttr('disabled')
        }
    });
}

function formatValue(value, format, prefix) {
    switch (format){
        case 'number':
            value = JSON.stringify(value);
            value = value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')+prefix;
            break;
        default:
            break;
    }

    return value;
}

function sliders() {
    let sliders = $('.forms__slides__slide');

    function setSlider(slider, minVal, maxVal, defaultVal, stepVal, output, format, prefix,elseInput=false){
        slider.slider({
            classes: {
                "ui-slider-handle": "forms__slides__slider__handle",
                "ui-slider-range": "forms__slides__slider__range"
            },
            range: "min",
            min: minVal,
            max: maxVal,
            value: defaultVal,
            step: stepVal,
            create: function() {
                let start_val = $(this).slider("value");

                output.text(formatValue(start_val, format, prefix));

                if(elseInput){
                    elseInput.val(start_val)
                }
            },
            slide: function (event, ui) {
                let val = ui.value;

                output.text(formatValue(val, format, prefix));

                if(elseInput){
                    elseInput.val(val)
                }
            }
        })
    }

    sliders.each(function () {
        let container = $(this);
        let slider = container.find('.forms__slides__slider');
        let output = container.find('.output>*:first-child');
        let input = container.find('input[type=hidden]')

        let minVal = parseInt(slider.attr('data-min'));
        let maxVal = parseInt(slider.attr('data-max'));
        let defaultVal = parseInt(slider.attr('data-default'));
        let stepVal = parseInt(slider.attr('data-step'));
        let format = slider.attr('data-format');
        let prefix = slider.attr('data-prefix');

        setSlider(slider, minVal, maxVal, defaultVal, stepVal, output, format, prefix, input);
    })
}

$(document).ready(function() {
    inputDropdown();
    inputDate();
    validate('#form_1');

    sliders();
});
