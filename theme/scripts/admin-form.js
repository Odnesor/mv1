jQuery(document).on('click', '#form_script_copy', function(event){
    event.preventDefault();
    let formScriptContainer = jQuery('.metabox_form_script');
    var tmp = jQuery("<textarea>");
    jQuery("body").append(tmp);
    tmp.val(formScriptContainer.text()).select();
    document.execCommand("copy");
    tmp.remove();
})
