<?php

namespace Multiform;

use Multiform\Type\Order;

class ApiRequest
{
	public static function send_lead_request( $data, $form_id, $logger ) {
		try {
			$data ['agree_with_terms'] = 'Есть согласие';
			$data ['product'] = get_field('product', $form_id) ?: 'PDL';

			$user_friendly_params = array_map( function ( $value, $name ) use ( $form_id ) {
				$value = $value ?: 'Не указан';
				$param_title = Order::get_user_friendly_param_title( $name, $form_id );
				if ( ! $param_title ) $param_title = $name;
				return "{$param_title}: {$value}";
			}, $data, array_keys( $data ) );

			$data ['full_text'] = implode( PHP_EOL, $user_friendly_params );

			$token = get_field( 'token', $form_id ) ?? '';
			if ( ! $token ) throw new \Exception( 'Missing token!' );

			$api_response = self::send( $token, $data, $logger );
			if ( is_a( $api_response, \Exception::class ) ) {
				throw $api_response;
			}
			return $api_response;
		} catch (\Exception $ex) {
			( $logger->generate_error_log() )->log( 'API SEND ERROR' . $ex->getMessage() );
			wp_send_json_error( [
				'apiErrorMessage' => $ex->getMessage()
			], 200 );
		}
	}

	public static function send( $token, $data, $logger ) {
		$url = 'https://api.beegl.net/api/';
		$request_data = [
			"method" => "leads.add",
			'params' => [
				"api_type_id" => 1,
				'phone'       => $data['cell_phone'],
				"data"        => $data
			]
		];

		$ch = curl_init();
		if ( ! $ch ) return false;

		curl_setopt_array( $ch, [
			CURLOPT_URL            => $url,
			CURLOPT_POST           => true,
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER     => [
				'Content-Type: application/json',
				'Token: ' . $token
			],
			CURLOPT_POSTFIELDS     => json_encode( $request_data ),
		] );

		$raw = curl_exec( $ch );
		$http_status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );


		$data = json_decode( $raw, true );
		if ( ! isset( $data['result'] ) ) {
			$errors_logger = $logger->generate_error_log();
			$errors_logger->log( PHP_EOL . 'REQUEST_PARAMS' . PHP_EOL . print_r( $request_data, true ) );
			$errors_logger->log( PHP_EOL . 'TOKEn' . PHP_EOL . $token );
			$errors_logger->log( PHP_EOL . 'RESPOnSE:' . print_r( $data, true ) );
			if ( $http_status_code !== 200 ) {
				$errors_logger->log( PHP_EOL . "HTTP_ERROR. STATUS CODE: {$http_status_code}" );
				return new \Exception( "HTTP_ERROR. STATUS CODE: {$http_status_code}. Read logs for more info" );
			}
			return new \Exception( "Read logs for more info" );
		}

		$logger = ( new Logger( 'api-requests-log' ) );
		$logger->log( PHP_EOL . 'REQUEST_PARAMS' . PHP_EOL . print_r( $request_data, true ) );
		$logger->log( PHP_EOL . 'TOKEn' . PHP_EOL . $token );
		$logger->log( PHP_EOL . 'RESPOnSE:' . print_r( $data, true ) );
		return $data;
	}
}