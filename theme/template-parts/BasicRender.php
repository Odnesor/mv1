<?php

abstract class BasicRender
{
	protected $fields_map = [];

	protected function reduce_pattern( $_pattern, $_fields_map ): string {
		$render = preg_replace_callback(

			'/\[\[((.|\n?)*)\]\]/',
			function ( $_loop ) use ( $_pattern, $_fields_map ) {
				$loop = $_loop[1];
				$wrapped_part = $_loop[0];
				if ( count( explode( '=>', $loop ) ) >= 2 ) {
					$fields_array_name = explode( '=>', $loop )[0];
					if ( ! isset( $_fields_map[ $fields_array_name ] ) ) {
						return $wrapped_part;
					};
					$part_pattern = implode( '=>',
						array_slice(
							explode( '=>', $loop ), 1
						) );
					$part_array_data = $_fields_map[ $fields_array_name ];

					$loop = array_map( function ( $data_item ) use ( $part_pattern ) {
						return $this->reduce_pattern( $part_pattern, $data_item );
					}, $part_array_data );
					return implode( '', $loop );
				} else {
					if ( isset( $_fields_map[ $loop ] ) ) {
						foreach ( $_fields_map[ $loop ] as $element ) {
							if ( ! is_string( $element ) ) return $wrapped_part;
						}
						return implode( '', $_fields_map[ $loop ] );
					}
				}
			},
			$_pattern );
		$render = preg_replace_callback(
			'/{{((.)*?)}}/',
			function ( $field ) use ( $_fields_map ) {
				$part = $field[1];
				$wrapped_part = $field[0];
				if ( isset( $_fields_map[ $part ] ) ) {
					return $_fields_map[ $part ];
				}
				return $wrapped_part;
			},
			$render );

		return $render;
	}

	abstract protected static function get_main_pattern(): string;

	public function get_render(): string {
		return $this->reduce_pattern( static::get_main_pattern(), $this->fields_map );
	}

	public function render() {
		echo $this->get_render();
	}
}