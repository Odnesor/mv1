<?php

class DateInputCustom extends DateInput
{
	public function __construct( $layout_acf ) {
		parent::__construct( $layout_acf );
		$this->fields_map['name'] = $layout_acf['text_group']['api_name'];
	}
}