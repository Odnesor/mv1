<?php

namespace Multiform;

class OrdersAdminTabs
{
	public static function init() {
		add_action( 'admin_menu', [__CLASS__, 'forms_admin_tabs'], 100 );
	}

	public static function forms_admin_tabs() {
		global $menu;

		$tabs_to_insert = array_map( function ( $term ) {
			$related_form_id = str_replace( 'form_', '', $term->slug );
			return [
				'Заявки ' . get_the_title( $related_form_id ),
				'edit_posts',
				"edit.php?related_form={$term->slug}&post_type=order",
				'',
				'',
				'',
				'dashicons-money'
			];
		}, get_terms( ['taxonomy' => 'related_form', 'hide_empty' => true] ) );

		$new_menu = [];
		foreach ( $menu as $menu_key => $tab ) {
			if ( $tab[2] == 'upload.php' ) continue;
			if ( $tab[2] == 'edit-comments.php' ) continue;
			if ( $tab[2] == 'edit.php' ) continue;
			if ( $tab[2] == 'edit.php?post_type=page' ) continue;
			foreach ( $tab as $param ) {
				if ( $param == 'edit.php?post_type=order' ) {
					$new_menu[ $menu_key ] = $tab;
					array_walk( $tabs_to_insert, function ( $tab, $count ) use ( &$new_menu, $menu_key ) {
						$count++;
						$new_menu["{$menu_key}_{$count}"] = $tab;
					} );
					break;
				}
			}
			$new_menu[ $menu_key ] = $tab;
		}
		$menu = $new_menu;
	}
}