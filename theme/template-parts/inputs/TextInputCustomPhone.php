<?php

class CustomPhone extends TextInput
{
	public function __construct( $layout_acf ) {
		parent::__construct( $layout_acf );
		$this->fields_map['name'] = $layout_acf['text_group']['api_name'];
		$this->fields_map['validation'] = 'tel';
	}
}