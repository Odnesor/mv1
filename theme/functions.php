<?php
define( 'TEMPLATE_PATH', get_template_directory_uri() . '/' );
define( 'AJAX_URL', admin_url( 'admin-ajax.php' ) );

class Theme
{
	private static $_instance;
	private        $core_classes = [
		\Multiform\InputTypesMap::class,
		\Multiform\FormACF::class,
		\Multiform\Logger::class,
		\Multiform\OrdersAdminTabs::class,

		\Multiform\Service\FormRequestHandler::class,

		\Bintime\Type\BasicType::class,
		\Multiform\Type\Form::class,
		\Multiform\Type\Order::class
	];

	public static function getInstance() {
		return self::$_instance ?? self::$_instance = new self;
	}

	private function __construct() {
		spl_autoload_register( [$this, 'spl_autoload_register'] );

		array_walk( $this->core_classes, function ( $class ) {
			if ( method_exists( $class, 'init' ) ) $class::init();
		} );

		$this->load_template_classes();

	}

	public function spl_autoload_register( $class_name ) {
		$dirs = explode( '\\', $class_name );
		if ( 'Multiform' == $dirs[0] ) {

			$path_templates = [
				'Page'    => '/includes/page/%1$s.php',
				'Type'    => '/includes/type/%1$s.php',
				'Service' => '/includes/service/%1$s.php',
			];
			if ( array_key_exists( $dirs[1], $path_templates ) ) {
				$path = __DIR__ . sprintf( $path_templates[ $dirs[1] ], $dirs[2] );

			} else {
				$path = __DIR__ . sprintf( '/includes/%1$s.php', $dirs[1] );
			}
			if ( file_exists( $path ) ) {
				include_once $path;
			}
		}
	}

	public function load_template_classes() {
		$classes = glob( __DIR__ . '/template-parts/*.php' );
		array_walk( $classes, function ( $class ) {
			include_once $class;
		} );
		$classes = glob( __DIR__ . '/template-parts/*/*.php' );
		array_walk( $classes, function ( $class ) {
			include_once $class;
		} );
	}
}

Theme::getInstance();