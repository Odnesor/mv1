<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="HandheldFriendly" content="true">
	<meta name="MobileOptimized" content="width">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">
	<title>Hello</title>
	<meta name="description" content="Simple demo app.">
	<link rel="stylesheet" href="<?= TEMPLATE_PATH ?>/styles/app.css">
	<script>
        let adminAJAX = '<?=AJAX_URL?>';
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

	<?php wp_head() ?>
</head>
<body>
<div class="app app_no_js">
	<!-- BEMGO:symbol -->
	<main class="content">