<?php

namespace Multiform;

class Logger
{
	public  $filename;
	public  $path;
	private $start_time;
	private $end_time;

	public function __construct( $file_name = 'request-log' ) {
		$uploads_dir = wp_get_upload_dir()['basedir'];
		$year = wp_date( 'Y' );
		$month = wp_date( 'm' );
		$day = wp_date( 'd' );
		$dir = "{$uploads_dir}/logs/{$year}/{$month}/{$day}";
		if ( ! is_dir( $dir ) ) {
			mkdir( $dir, 0777, true );
		}
		$this->filename = $file_name ?: 'log';
		$this->path = "{$dir}/" . $this->filename . ".log";
		if ( ! file_exists( $this->path ) ) file_put_contents( $this->path, '' );
	}

	public function generate_suffixed_log( $_suffix ): Logger {
		$log_name = "{$this->filename}-{$_suffix}";
		return new Logger( $log_name );
	}

	public function generate_error_log(): Logger {
		return $this->generate_suffixed_log( 'errors' );
	}

	public function log( $message ) {
		if ( is_array( $message ) ) {
			$message = print_r( $message, true );
		}
		file_put_contents(
			$this->path,
			PHP_EOL . date( 'H:i:s' ) . PHP_EOL . $message . PHP_EOL,
			FILE_APPEND
		);
	}

	public function get_file() {
		return file_get_contents( $this->path );
	}

	public function clear() {
		file_put_contents(
			$this->path,
			''
		);
	}

	public function track_start() {
		$this->start_time = microtime( true );
	}

	public function track_end() {
		$this->end_time = microtime( true );
	}

	public function get_log_time_interval( $time_wrapper = '' ) {
		$interval = ( $this->end_time - $this->start_time );
		if ( $time_wrapper ) {
			$interval = str_replace(
				'{time}',
				$interval,
				$time_wrapper
			);
		}

		if ( floatval( $interval ) > 1 ) {
			$interval = '>1s | ' . $interval;
		}
		return $interval;
	}

	public function log_time_interval( $time_wrapper = '' ) {
		$interval = $this->get_log_time_interval();
		$this->log( $interval );
	}

	public function sum_up_wrapped_nums( $regex ) {
		$sum = 0;
		preg_replace_callback( $regex, function ( $num ) use ( &$sum ) {
			$sum += floatval( $num[1] );
			return $num;
		}, $this->get_file() );

		return print_r( $sum, true );
	}
}
