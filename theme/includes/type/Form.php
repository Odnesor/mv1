<?php

namespace Multiform\Type;

use Multiform\FormACF;
use Multiform\InputTypesMap;

class Form extends BasicType
{
	const POST_TYPE = 'form';

	protected static $register_args = [
		'label'        => 'Форма',
		'labels'       => [
			'name'          => 'Форма',
			'singular_name' => 'Форма',
			'menu_name'     => 'Зарегистрированые формы',
			'add_new'       => 'Зарегистрировать форму',
			'add_new_item'  => 'Сгенерировать разметку',
			'update_item'   => 'Сгенерировать',
			'edit_item'     => 'Изменить параметры формы',
			'new_item'      => 'Зарегистрировать новую форму',
		],
		'public'       => true,
		'show_ui'      => true,
		'menu_icon'    => 'dashicons-forms',
		'show_in_menu' => true,
		'hierarchical' => false,
		'supports'     => ['title'],
		'taxonomies'   => [],
		'has_archive'  => false,
		'rewrite'      => true,
		'query_var'    => true,
	];

	public static function init() {
		add_action( 'init', [__CLASS__, 'register_post_type'] );
		add_action( 'acf/init', [FormACF::class, 'acf_add_local_field_group'] );
		add_action( 'acf/init', [__CLASS__, 'acf_add_options_page'] );
		add_action( 'add_meta_boxes', function () {
			add_meta_box( 'form-html-meta-box', 'Разметка формы', [__CLASS__, 'render_my_meta_form_script_metabox'], 'form', 'side' );
		} );

		add_action( 'admin_enqueue_scripts', function () {
			if ( wp_is_mobile() ) return;
			global $pagenow;
			if ( ( in_array( $pagenow, ['post.php', 'post-new.php'] ) ) && ( get_post()->post_type == Form::POST_TYPE ) ) {
				wp_enqueue_style( 'admin-form-type', TEMPLATE_PATH . '/styles/admin-form-type.css' );
				wp_enqueue_script( 'admin-form-type', TEMPLATE_PATH . '/scripts/admin-form.js' );
			}
		} );
	}

	public static function render_my_meta_form_script_metabox() {
		( new \FormInputScript( get_the_ID() ) )->render();
	}

	public static function check_form_existance( $form_id ) {
		if ( ! $form_id && is_user_logged_in() ) {
			$posts = get_posts( ['post_type' => Form::POST_TYPE, 'fields' => 'ids'] );
			if ( empty( $posts ) ) return false;
			return $posts[0];
		}
		if ( ! ( get_post( $form_id ) ) ) return false;
		if ( ! get_post( $form_id )->post_type == self::POST_TYPE ) return false;
		if ( get_post( $form_id )->post_status == 'trash' ) return false;
		return $form_id;
	}

	public static function track_form_init( $wp_form_id, $site_url ) {
		if ( ! get_field( 'is_inited', $wp_form_id ) ) {
			wp_insert_term( get_the_title( $wp_form_id ), 'related_form', ['slug' => 'form_' . $wp_form_id] );
			update_field( 'is_inited', true, $wp_form_id );
		}
		update_field( 'last_site', $site_url, $wp_form_id );
	}

	public static function register_post_type() {
		parent::register_post_type();
	}

	public static function acf_add_options_page() {
		if ( function_exists( 'acf_add_options_page' ) ) {

			acf_add_options_page( array (
				'page_title' => 'Стандартные настройки',
				'menu_title' => 'Стандартные настройки',
				'menu_slug'  => 'form-settings',
				'parent'     => 'edit.php?post_type=form',
				'capability' => 'edit_posts',
				'redirect'   => false,
			) );

			acf_add_local_field_group( array (
				'key'                   => 'group_options61dff5c7a9996',
				'title'                 => 'Заявка',
				'fields'                => array (
					array (
						'key'               => 'field_options_61dee813b7853',
						'label'             => 'Заголовок',
						'name'              => 'title',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array (
						'key'               => 'field_optioins_61dee81eb7854',
						'label'             => 'Подзаголовок',
						'name'              => 'subtitle',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array (
						'key'               => 'field_options_agreement61dfed3f64f67',
						'label'             => 'Согласие',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array (
						'key'               => 'field_options_61dfe67bbf32d',
						'label'             => 'Согласие',
						'name'              => 'agreement',
						'type'              => 'wysiwyg',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => 'links-wysiwyg',
							'id'    => '',
						),
						'default_value'     => '',
						'tabs'              => 'all',
						'toolbar'           => 'basic',
						'media_upload'      => 0,
						'delay'             => 0,
					),
					array (
						'key'               => 'field_options_button61dfed3f64f67',
						'label'             => 'Кнопка "далее"',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array (
						'key'               => 'field_options_button_text61dd8681b1748',
						'label'             => 'Кнопка "Далее"',
						'name'              => 'button_text',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => 'Далее',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array (
						'key'               => 'field_options_success61dfed3f64f67',
						'label'             => 'Текст успешной отправки',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array (
						'key'               => 'field_options_success_text61dd8681b1748',
						'label'             => 'Текст успешной отправки',
						'name'              => 'success_text',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => 'Ваши данные успешно отправлены!',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
				),
				'location'              => array (
					array (
						array (
							'param'    => 'options_page',
							'operator' => '==',
							'value'    => 'form-settings',
						),
					),
				),
				'menu_order'            => 0,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => true,
				'description'           => '',
			) );
		}
	}
}