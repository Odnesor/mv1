<?php

namespace Multiform;

class InputTypesMap
{
	const MAP = [
		'first_name'            => [
			'acf'                 => 'Имя',
			'type'                => 'text',
			'validation'          => 'cyrillic_space',
			'maxlength'           => 20,
			'default_placeholder' => 'Анатолий'
		],
		'last_name'             => [
			'acf'                 => 'Фамилия',
			'type'                => 'text',
			'validation'          => 'cyrillic_space',
			'maxlength'           => 20,
			'default_placeholder' => 'Красовский-Приймаковский'
		],
		'middle_name'           => [
			'acf'                 => 'Отчество',
			'type'                => 'text',
			'validation'          => 'cyrillic_space',
			'maxlength'           => 20,
			'default_placeholder' => 'Константинович'
		],
		'birth_date'            => [
			'acf'                 => 'Дата рождения',
			'validation'          => 'birth_date',
			'type'                => 'date',
			'default_placeholder' => false
		],
		'cell_phone'            => [
			'acf'                 => 'Мобильный телефон',
			'type'                => 'text',
			'validation'          => 'tel',
			'default_placeholder' => '+7(9XX)XXX-XX-XX'
		],
		'email'                 => [
			'acf'                 => 'E-mail',
			'type'                => 'text',
			'validation'          => 'email',
			'default_placeholder' => 'creditsum1678@gmail.com'
		],
		'city_fact'             => [
			'acf'                 => 'Город текущего проживания',
			'type'                => 'text',
			'validation'          => 'cyrillic_defis_space',
			'maxlength'           => 30,
			'default_placeholder' => 'Ростов-на-дону'
		],
		'passport'              => [
			'type'   => 'double',
			'acf'    => 'Паспорт',
			'inputs' => [
				[
					'name'                => 'passport_seria',
					'acf'                 => 'Серия паспорт',
					'validation'          => 'passport_series',
					'maxlength'           => 10,
					'default_placeholder' => 'XX XX'
				],
				[
					'name'                => 'passport_num',
					'acf'                 => 'Номер паспорта',
					'validation'          => 'passport_num',
					'maxlength'           => 10,
					'default_placeholder' => 'XXXXXX'
				],
			]
		],
		'passport_code'         => [
			'acf'                 => 'Код подразделения',
			'type'                => 'text',
			'validation'          => 'passport_code',
			'maxlength'           => 10,
			'default_placeholder' => 'XXX-XXX'
		],
		'passport_issued'       => [
			'acf'                 => 'Кем выдан',
			'type'                => 'text',
			'validation'          => 'cyrillic_nums_apostrof_defis_space',
			'maxlength'           => 100,
			'default_placeholder' => 'Организация выдавшая паспорт'
		],
		'passport_date'         => [
			'acf'                 => 'Дата выдачи паспорта',
			'validation'          => 'passport_date',
			'type'                => 'date',
			'default_placeholder' => false
		],
		'birth_place'           => [
			'acf'                 => 'Место рождения',
			'type'                => 'text',
			'validation'          => 'cyrillic_nums_defis_space',
			'maxlength'           => 30,
			'default_placeholder' => 'Например: Москва'
		],
		'region_registration'   => [
			'acf'                 => 'Регион',
			'type'                => 'text',
			'validation'          => 'cyrillic_apostrof_defis_space',
			'maxlength'           => 30,
			'default_placeholder' => 'Например: Московский'
		],
		'city_registration'     => [
			'acf'                 => 'Город',
			'type'                => 'text',
			'validation'          => 'cyrillic_apostrof_defis_space',
			'maxlength'           => 30,
			'default_placeholder' => 'Например: Москва'
		],
		'street_registration'   => [
			'acf'                 => 'Улица',
			'type'                => 'text',
			'validation'          => 'cyrillic_apostrof_defis_space',
			'maxlength'           => 30,
			'default_placeholder' => 'Например: Московская'
		],
		'house_registration'    => [
			'acf'                 => 'Дом',
			'type'                => 'text',
			'validation'          => 'nums_letters_special',
			'maxlength'           => 5,
			'default_placeholder' => 'Укажите номер дома'
		],
		'building_registration' => [
			'acf'                 => 'Строение / корпус',
			'type'                => 'text',
			'validation'          => 'nums_letters_special',
			'maxlength'           => 5,
			'default_placeholder' => 'Укажите номер корпуса'
		],
		'kv_registration'       => [
			'acf'                 => 'Квартира',
			'type'                => 'text',
			'validation'          => 'nums_letters_special',
			'maxlength'           => 5,
			'default_placeholder' => 'Например 56'
		],
		'amount'                => [
			'acf'         => 'Сумма',
			'type'        => 'range',
			'default'     => '1000',
			'min'         => 1000,
			'max'         => 3000000,
			'step'        => 1000,
			'units'       => 'рублей',
			'units_short' => 'руб.'
		],
		'period_days'           => [
			'acf'         => 'Период',
			'type'        => 'range',
			'default'     => '365',
			'min'         => 1,
			'max'         => 1825,
			'step'        => 1,
			'units'       => 'недели',
			'units_short' => 'нед.'
		],
	];
}