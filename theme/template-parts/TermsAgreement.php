<?php

class TermsAgreement extends BasicRender
{
	public function __construct( $form_id ) {
		if ( get_field( 'agreement', $form_id ) ) {
			$text = strip_tags( get_field( 'agreement', $form_id ), '<a>' );
		} else {
			$text = strip_tags( get_field( 'agreement', 'options' ), '<a>' );
		}
		$this->fields_map = [
			'text' => $text
		];
	}

	public static function get_main_pattern(): string {
		return '<div class="forms__block__rows__item center">
					<div class="form-input   checkbox ">
						<input type="checkbox" id="agree_with_terms" name="agree_with_terms" placeholder="" data-validation="" required>
						<label for="agree_with_terms">
							<p>{{text}}</p>
						</label>
						<div class="form-input__error"></div>
					</div>
				</div>';
	}
}