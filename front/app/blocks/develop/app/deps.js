
'use strict'

module.exports = {

	nodes: [],

	modules: [
		{
			import: 'normalize.css'
		},
		{
			from: 'app/blocks/develop/plugins/inputmask/',
			inject: [ 'jquery.inputmask.min.js' ],
		},
		{
			from: 'app/blocks/develop/plugins/validate/',
			inject: [ 'jquery.validate.min.js' ],
		},
		{
			from: 'app/blocks/develop/plugins/validate/',
			inject: [ 'additional-methods.min.js' ]
		}
	],

}
