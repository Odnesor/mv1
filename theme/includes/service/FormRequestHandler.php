<?php

namespace Multiform\Service;

use Multiform\ApiRequest;
use Multiform\InputTypesMap;
use Multiform\Logger;
use Multiform\Type\Order;

class FormRequestHandler
{
	const NAMESPACE = 'form';
	const ROUTE     = 'send';

	public static function init() {
		add_action( 'rest_api_init', function () {
			register_rest_route( self::NAMESPACE, self::ROUTE, array (
				'methods'  => 'POST',
				'callback' => [__CLASS__, 'request_handler'],
			) );
		} );
	}

	public static function get_route(): string {
		return self::NAMESPACE . "/" . self::ROUTE;
	}

	public static function get_url(): string {
		return get_rest_url() . self::get_route();
	}

	public static function request_handler() {
		$data = $_REQUEST;
		$form_id = $data['form_id'];
		$logger = new Logger( str_replace( ' ', '-', get_the_title( $form_id ) ) );
		self::format_data( $data );

		Order::save_in_admin( $data, $form_id, $logger );
		$request_response = ApiRequest::send_lead_request( $data, $form_id, $logger );
		wp_send_json_success( [
			'message' => $request_response
		] );
	}

	private static function format_data( &$data ) {
		$formatted_data = [];
		$form_id = $data['form_id'];
		$disabled_params = array_map(function($layout){
			if($layout['settings_group']['is_disable']){
				return $layout['text_group']['api_name'] ?? $layout['acf_fc_layout'] ;
			}else{
				return '';
			}
		}, get_field('form', $form_id));
		$disabled_params = array_merge($disabled_params, ['api_form_handler', 'form_id']);
		foreach ( $data as $name => $value ) {
			if(in_array($name, $disabled_params)) continue;
			preg_match( "/(.*)?_(day|year|month)$/", $name, $date );
			if ( $date ) {
				$param_name = $date[1];
				$day = $data["{$param_name}_day"];
				$month = $data["{$param_name}_month"];
				$year = $data["{$param_name}_year"];
				$formatted_data[ $param_name ] = "{$year}-{$month}-{$day}";
				unset( $day, $month, $year );
				continue;
			}
			if ( $name == 'cell_phone' ) $value = preg_replace( '/[^0-9.]+/', '', $value );
			if ( $name == 'agree_with_terms' ) {
				$formatted_data['Согласие'] = 'Есть согласие';
				continue;
			}

			$formatted_data[ $name ] = $value;
		}
		$data = $formatted_data;
	}
}