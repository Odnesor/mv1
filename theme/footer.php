</main>
<script type="text/javascript">
    var message = {
        empty: 'Это поле обязательное',
        not_enough: 'Недостаточно символов',
        max_length: 'Маскимальная длина: ',
        min_length: 'Минимальная длина: ',
        min_age: 'Вам должно быть больше 18',
        max_age: 'Ваш возраст слишком большой',
        lettersonly: 'Это поле принимает только буквы',

        cyrillic: 'Допускается только кириллица',
        cyrillic_space: 'Допускается только кириллица',
        cyrillic_defis: 'Допускается кирилица и дефис',
        cyrillic_defis_space: 'Допускается кирилица и дефис',
        cyrillic_apostrof: 'Допускается кирилица и апостроф',
        cyrillic_apostrof_space: 'Допускается кирилица и апостроф',
        cyrillic_apostrof_defis: 'Допускается кирилица, дефис и апостроф',
        cyrillic_apostrof_defis_space: 'Допускается кирилица, дефис и апостроф',

        cyrillic_nums: 'Допускается кириллица и цифры',
        cyrillic_nums_space: 'Допускается кириллица и цифры',
        cyrillic_nums_defis: 'Допускается кириллица, дефис и цифры',
        cyrillic_nums_defis_space: 'Допускается кириллица, дефис и цифры',
        cyrillic_nums_apostrof: 'Допускается кирилица, апостроф и цифры',
        cyrillic_nums_apostrof_space: 'Допускается кирилица, апостроф и цифры',
        cyrillic_nums_apostrof_defis: 'Допускается кирилица, дефис, апостроф и цифры',
        cyrillic_nums_apostrof_defis_space: 'Допускается кирилица, дефис, апостроф и цифры',

        nums_letters: 'Допускаются только цифры и буквы',
        nums_letters_space: 'Допускаются только цифры и буквы',
        nums_letters_special: 'Допускаются цифры, буквы и спец.символы',

        only_num: 'Допускаются только цифры',
        nums_space_defis: 'Допускаются цифры, буквы и пробел',

        email: 'Введите корректный email',
        passport_date: 'Введите корректную дату'
    }
    var months_array = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
</script>
</div>
<script src="<?= TEMPLATE_PATH ?>scripts/jquery.inputmask.min.js"></script>
<script src="<?= TEMPLATE_PATH ?>scripts/jquery.validate.min.js"></script>
<script src="<?= TEMPLATE_PATH ?>scripts/additional-methods.min.js"></script>
<script src="<?= TEMPLATE_PATH ?>scripts/jquery-ui.min.js"></script>
<script src="<?= TEMPLATE_PATH ?>scripts/jquery.ui.touch-punch.min.js"></script>
<script src="<?= TEMPLATE_PATH ?>scripts/app.js"></script>

<?php wp_footer() ?>

</body>
</html>