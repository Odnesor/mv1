<?php

namespace Multiform\Type;

class BasicType
{
	const POST_TYPE = '';
	protected static $register_args;

	public static function register_post_type() {
		if ( ! static::POST_TYPE ) return;

		register_post_type( static::POST_TYPE, static::$register_args );
	}

	public static function get_by( $args = [] ) {
		$default_args = [
			'post_type'   => static::POST_TYPE,
			'numberposts' => -1,
			'fields'      => 'ids',
			'order'       => 'ASC'
		];

		return get_posts( array_merge( $default_args, $args ) );
	}
}
