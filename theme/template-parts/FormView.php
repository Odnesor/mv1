<?php

use Multiform\InputTypesMap;
use Multiform\Service\FormRequestHandler;
use Multiform\Service\InitRequestHandler;

class FormView extends BasicRender
{
	const INPUTS_RENDER_MAP        = [
		'text'   => TextInput::class,
		'date'   => DateInput::class,
		'double' => DoubleTextInput::class,
		'range'  => RangeInput::class,
	];
	const CUSTOM_INPUTS_RENDER_MAP = [
		'custom_text'  => TextInputCustom::class,
		'custom_date'  => DateInputCustom::class,
		'custom_phone' => CustomPhone::class,
		'custom_num'   => CustomNumber::class,
	];

	public function __construct( $form_id ) {
		$button_text = get_field( 'button_text', $form_id ) ?: ( get_field( 'button_text', 'options' ) ?? '' );
		$success_text = get_field( 'success_text', $form_id ) ?: ( get_field( 'success_text', 'options' ) ?? '' );

		$this->fields_map = [
			'title'            => get_field( 'title', $form_id ) ?: ( get_field( 'title', 'options' ) ?? '' ),
			'subtitle'         => get_field( 'subtitle', $form_id ) ?: ( get_field( 'subtitle', 'options' ) ?? '' ),
			'blocks'           => $this->split_layouts_by_blocks( $form_id ),
			'button_text'      => $button_text ?: 'Далее',
			'success_text'     => $success_text ?: 'Ваши данные успешно отправлены!',
			'site'             => $_GET['site_url'] ?? '',
			'api_form_handler' => FormRequestHandler::get_url(),
			'form_id'          => $form_id,
			'ip'               => $_SERVER['REMOTE_ADDR'],
			'TEMPLATE_PATH'    => TEMPLATE_PATH
		];
	}

	public function split_layouts_by_blocks( $form_id ): array {
		$_layouts = get_field( 'form', $form_id ) ?: [];
		$blocks = [];
		foreach ( $_layouts as $count => $layout ) {
			$layout_name = $layout['acf_fc_layout'];
			$prev_layout = $_layouts[ $count - 1 ] ?? '';
			$next_layout = $_layouts[ $count + 1 ] ?? '';
			if ( self::is_range_layout( $layout ) ) {
				$blocks [] = $this->get_slides_render( $layout, $prev_layout, $next_layout );
				continue;
			}
			if ( ( ( $count == 0 ) || self::is_range_layout( $prev_layout ) ) && ( 'section_delimiter' != $layout_name ) ) {
				$blocks[] = ['block_title' => ''];
			}
			if ( 'section_delimiter' == $layout_name ) {
				$blocks[] = ['block_title' => $layout['title'] ?? ''];
				if ( ! $next_layout || self::is_range_layout( $next_layout ) || (isset($next_layout['acf_fc_layout']) && ($next_layout['acf_fc_layout'] == 'section_delimiter')) ) {
					$this->generate_block_render( $blocks[ array_key_last( $blocks ) ] );
				}
				continue;
			}

			$block_renderer = self::assign_layout_renderer( $layout_name );
			if ( ! $block_renderer ) continue;

			$current_block_index = array_key_last( $blocks );
			$blocks[ $current_block_index ]['inputs'][] = ( new $block_renderer( $layout ) )->get_render();
			if ( ! $next_layout ) {
				$blocks[ $current_block_index ]['inputs'][] = ( new TermsAgreement( $form_id ) )->get_render();
			}
			if ( ! $next_layout || ( $next_layout['acf_fc_layout'] == 'section_delimiter' ) || self::is_range_layout( $next_layout ) ) {
				$this->generate_block_render( $blocks[ $current_block_index ] );
			}
		}
//		if(!isset($blocks[ array_key_last( $blocks ) ]['block_title'])){
//			$blocks[ array_key_last( $blocks ) ]['block_title'] = '';
//		}
		return $blocks;
	}


	private static function assign_layout_renderer( $layout_name ) {
		if ( isset( InputTypesMap::MAP[ $layout_name ] ) ) {
			$layout_input_type = InputTypesMap::MAP[ $layout_name ]['type'];
			$renderer = self::INPUTS_RENDER_MAP[ $layout_input_type ] ?? '';
			if ( ! $renderer ) return '';
		} elseif ( isset( self::CUSTOM_INPUTS_RENDER_MAP[ $layout_name ] ) ) {
			$renderer = self::CUSTOM_INPUTS_RENDER_MAP[ $layout_name ];
		} else return '';

		return $renderer;
	}


	private static function is_range_layout( $layout ): bool {
		if ( isset( $layout['acf_fc_layout'] ) ) {
			$layout_name = $layout['acf_fc_layout'];
			if (
				isset( InputTypesMap::MAP[ $layout_name ] ) &&
				isset( InputTypesMap::MAP[ $layout_name ]['type'] ) &&
				( 'range' == InputTypesMap::MAP[ $layout_name ]['type'] )
			) return true;
		}

		return false;
	}

	public function get_slides_render( $layout, $prev, $next ): string {
		if($layout['settings_group']['disable']) return '';
		$sliders = [$layout];
		if ( $prev && self::is_range_layout( $prev ) && !$prev['settings_group']['disable']) return '';
		if ( $next && self::is_range_layout( $next ) && !$next['settings_group']['disable']) $sliders [] = $next;

		$fields = [
			'slides' => array_map( function ( $layout ) {
				return [
					'name'       => $layout['acf_fc_layout'],
					'title'       => $layout['text_group']['title'],
					'min'         => $layout['text_group']['min'],
					'max'         => $layout['text_group']['max'],
					'default'     => $layout['text_group']['default'],
					'step'        => $layout['text_group']['step'],
					'units'       => $layout['text_group']['units'],
					'units_short' => $layout['text_group']['units_short'],
				];
			}, $sliders )
		];
		$pattern = '<div class="forms__block forms__slides">
						[[slides=>
						<div class="forms__slides__slide">
							<div class="forms__slides__label">
								<p>{{title}}</p>
								<p class="output"><span></span><span></span></p>
							</div>
							<div class="forms__slides__slider" 
								data-min="{{min}}" 
								data-max="{{max}}" 
								data-default="{{default}}" 
								data-step="{{step}}" 
								data-format="number" 
								data-prefix=" {{units_short}} ">
							</div>
							<div class="forms__slides__text">
								<p>{{min}} {{units}}</p>
								<p>{{max}} {{units}}</p>
							</div>
							<input type="hidden" name="{{name}}" id="{{name}}">
						</div>]]
					</div>';
		return $this->reduce_pattern( $pattern, $fields );
	}

	public function generate_block_render( &$block ) {
		$pattern = '<div class="forms__block">
						<div class="forms__block__title">
							<p>{{block_title}}</p>
						</div>
						<div class="forms__block__rows">
							[[inputs]]
						</div>
					</div>';
		$block = $this->reduce_pattern( $pattern, $block );
	}

	public static function get_main_pattern(): string {
		return '<div class="wrapper">
					<div class="forms">
						<div class="section-title">
							<h2>{{title}}</h2>
						</div>
						<div class="section-subtitle">
							<p>{{subtitle}}</p>
						</div>
						<form id="form_1" name="form_1">
							<input type="hidden" name="api_form_handler" value="{{api_form_handler}}">
							<input type="hidden" name="form_id" value="{{form_id}}">
							<input type="hidden" name="ip" value="{{ip}}">
							<input type="hidden" name="site" value="{{site}}">
							[[blocks]]
							<div class="forms__block">
								<div class="forms__block__rows">
									<div class="forms__block__rows__item center">
										<div class="form-button  color_primary">
											<button type="submit" for="form_1">{{button_text}}</button>
										</div>
									</div>
								</div>
							</div>
							<div class="forms__block forms__success">
								<img src="{{TEMPLATE_PATH}}/static/app/img/success_check.png">
								<p>{{success_text}}</p>
							</div>
						</form>
					</div>
				</div>';
	}

	public static function get_unregistered_form_error_render() {
		return '<div class="wrapper">
					<div class="forms">
						<div class="section-title">
							<h2>Вы используете незарегистрованную форму!</h2>
						</div>
						<div class="section-subtitle">
							<p>Зарегистрируйте вашу форму на <a href="' . $_SERVER['HTTP_HOST'] . '">' . $_SERVER['HTTP_HOST'] . '</a></p>
						</div>
					</div>
				</div>
				';
	}
}