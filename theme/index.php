<?php
get_header();
$form_id = Multiform\Type\Form::check_form_existance( $_GET['form_id'] ?? '' );
if ( $form_id ) {
	( new FormView( $form_id ) )->render();
	Multiform\Type\Form::track_form_init( $_GET['form_id'] ?? '', $_GET['site_url'] ?? '' );
} else {
	echo FormView::get_unregistered_form_error_render();
}
get_footer();
