<?php

namespace Multiform\Type;

use Multiform\InputTypesMap;

class Order extends BasicType
{
	const POST_TYPE = 'order';

	protected static $register_args = [
		'label'        => 'Заявка',
		'labels'       => [
			'name'          => 'Заявка',
			'singular_name' => 'Заявка',
			'menu_name'     => 'Все заявки',
		],
		'public'       => false,
		'show_ui'      => true,
		'menu_icon'    => 'dashicons-money',
		'show_in_menu' => true,
		'hierarchical' => false,
		'supports'     => ['title'],
		'taxonomies'   => [],
		'has_archive'  => false,
		'rewrite'      => true,
		'query_var'    => true,
	];

	public static function init() {
		add_action( 'init', [__CLASS__, 'register_post_type'] );
		add_action( 'init', [__CLASS__, 'register_taxonomy'] );
		add_action( 'acf/init', [__CLASS__, 'acf_add_local_field_group'] );
		add_action( 'wp_trash_post', [__CLASS__, 'delete_apropriate_form_term'], 10, 1 );
	}

	public static function register_post_type() {
		parent::register_post_type();
	}

	public static function delete_apropriate_form_term( $post_id ) {
		if ( get_post( $post_id )->post_type == Form::POST_TYPE ) {
			$term_id = get_term_by( 'slug', "form_{$post_id}", 'related_form' )->term_id;
			wp_delete_term( $term_id, 'related_form' );
		}
	}

	public static function save_in_admin( $data, $form_id, $logger ): string {
		$list = array_map( function ( $param_name ) use ( $data, $form_id ) {
			$value = $data[ $param_name ] ?: 'Не указан';
			$param_title = self::get_user_friendly_param_title( $param_name, $form_id );
			if ( ! $param_title ) $param_title = $param_name;
			return ['name' => $param_title, 'value' => $value];
		}, array_keys( $data ) );

		$args = [
			'post_type'   => Order::POST_TYPE,
			'post_title'  => $data['first_name'] . ' ' . $data['last_name'],
			'post_status' => 'publish'
		];

		$post_id = wp_insert_post( $args );
		if ( ! $post_id ) return '';
		update_field( 'list', array_filter( $list ), $post_id );
		wp_set_post_terms( $post_id, "form_{$form_id}", 'related_form' );
		if ( is_a( $post_id, 'WP_Error' ) ) {
			( $logger->generate_suffixed_log( 'wp-errors' ) )->log( $post_id );
			wp_send_json_error( [
				'saveAdminError' => $post_id->get_error_message
			], 200 );
		}
		return $post_id;
	}

	public static function get_user_friendly_param_title( $param_name, $form_id ): string {
		$param_title = self::find_title_in_layouts( $param_name, $form_id );
		if ( ! $param_title ) $param_title = self::find_title_in_inputs_map( $param_name );
		if ( ! $param_title ) $param_title = self::find_custom_input_title( $param_name, $form_id );
		return $param_title;
	}

	private static function find_title_in_inputs_map( $input_name ) {
		foreach ( InputTypesMap::MAP as $name => $settings ) {
			if ( $input_name == $name ) return $settings['acf'];
			if ( $settings['type'] == 'double' ) {
				if ( $settings['inputs'][0]['name'] == $input_name ) {
					return $settings['inputs'][0]['acf'];
				}
				if ( $settings['inputs'][1]['name'] == $input_name ) {
					return $settings['inputs'][1]['acf'];
				}
			}
		}
		return '';
	}

	private static function find_title_in_layouts( $input_name, $form_id ) {
		$acf_layouts = get_field( 'form', $form_id );
		foreach ( $acf_layouts as $layout ) {
			if ( $input_name == $layout['acf_fc_layout'] ) return $layout['title'];
			if ( isset( $layout['first_input'] ) && ( $input_name == $layout['first_input']['title'] ) ) {
				return $layout['first_input']['title'];
			}
			if ( isset( $layout['second_input'] ) && ( $input_name == $layout['second_input']['title'] ) ) {
				return $layout['second_input']['title'];
			}
		}
	}

	public static function find_custom_input_title( $param_name, $form_id ) {
		foreach ( get_field( 'form', $form_id ) as $input ) {
			if ( in_array( $input['acf_fc_layout'], InputTypesMap::MAP ) ) continue;
			if ( $input['name'] == $param_name ) return $input['title'];
		}

		return '';
	}

	public static function register_taxonomy() {
		register_taxonomy( 'related_form', self::POST_TYPE, array (
			'labels'  => array (
				'name'          => 'Формы',
				'singular_name' => 'Форма',
			),
			'show_ui' => true,
		) );
	}

	public static function acf_add_local_field_group() {
		if ( function_exists( 'acf_add_local_field_group' ) ):

			acf_add_local_field_group( array (
				'key'                   => 'group_61dff5c7a9996',
				'title'                 => 'Заявка',
				'fields'                => array (
					array (
						'key'               => 'field_61dff5d08ec1a',
						'label'             => '',
						'name'              => 'list',
						'type'              => 'repeater',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array (
							'width' => '',
							'class' => 'form_params',
							'id'    => '',
						),
						'collapsed'         => '',
						'min'               => 0,
						'max'               => 0,
						'layout'            => 'table',
						'button_label'      => '',
						'sub_fields'        => array (
							array (
								'key'               => 'field_61e001d625a8a',
								'label'             => 'Имя поля',
								'name'              => 'name',
								'type'              => 'text',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array (
									'width' => '50',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
							array (
								'key'               => 'field_61e001ee25a8b',
								'label'             => 'Значение',
								'name'              => 'value',
								'type'              => 'text',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array (
									'width' => '50',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
						),
					),
				),
				'location'              => array (
					array (
						array (
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'order',
						),
					),
				),
				'menu_order'            => 0,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => true,
				'description'           => '',
			) );

		endif;
	}
}