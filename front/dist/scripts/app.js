/**
 * Main validation form
 * @param {form} jquery obj - Form
 * @param {options} obj - object width params
 */

$.validator.setDefaults({
    ignore: []
});

const only_num = /^[0-9]+$/;
const nums_space = /^[0-9\s]+$/;
const nums_defis = /^[0-9\-]+$/;

const cyrillic = /^[аА-яЯіІєЄїЇґҐ]+$/i;
const cyrillic_defis = /^[аА-яЯіІєЄїЇґҐ\-]+$/i;
const cyrillic_apostrof = /^[аА-яЯіІєЄїЇґҐ\'\`]+$/i;
const cyrillic_apostrof_defis = /^[аА-яЯіІєЄїЇґҐ\'\`\-]+$/i;
const cyrillic_apostrof_space = /^[аА-яЯіІєЄїЇґҐ\'\`\s]+$/i;
const cyrillic_apostrof_space_defis = /^[аА-яЯіІєЄїЇґҐ\'\`\"\s\-]+$/i;


const nums_letters = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9]*$/;
const nums_letters_defis = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9\-]*$/;
const nums_letters_space = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9\s]*$/;
const nums_letters_space_defis = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9\s\-]*$/;
const nums_letters_space_quote = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9\s\'\`\"]*$/;
const nums_letters_space_quote_defis = /^[a-zA-Zа-яієїґА-ЯІЄЇҐ0-9\s\'\`\"\-\,\.]*$/;

const email_reg = /^(([^<>()\[\]\\.,;:\s@"]{2,62}(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z-аА-яЯ\-0-9]+\.)+[a-zA-Z-аА-яЯ]{2,62}))$/;

const mobile_regex = /^[+]*[0-9]{1,2}\s[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\.\/0-9]*$/g;
const mobile_regex2 = /^[0-9\(\)\+]+$/;
const mobile_mask = '+7(\\999)999-99-99';
const mobile_mask_option = {placeholder: "X", autoclear: false};

const inputs = {
    'first_name': {
        'rules': {
            regex: cyrillic,
            maxlength: 20,
            messages: {
                regex: message.cyrillic,
                maxlength: message.max_length + 20
            }
        }
    },
    'middle_name': {
        'rules': {
            regex: cyrillic,
            maxlength: 20,
            messages: {
                regex: message.cyrillic,
                maxlength: message.max_length + 20
            }
        }
    },
    'last_name': {
        'rules': {
            regex: cyrillic,
            maxlength: 20,
            messages: {
                regex: message.cyrillic,
                maxlength: message.max_length + 20
            }
        }
    },

    'email': {
        'rules': {
            regex: email_reg,
            maxlength: 320,
            messages: {
                regex: message.email,
                maxlength: message.max_length + 320
            }
        }
    },

    'birth_date': {
        'rules': {
            check_date_of_birth: true
        }
    },

    'city': {
        'rules': {
            regex: cyrillic_apostrof,
            maxlength: 30,
            messages: {
                regex: message.cyrillic_apostrof,
                maxlength: message.max_length + 30
            }
        }
    },

    'passport_series': {
        'rules': {
            regex: nums_space,
            messages: {
                regex: message.not_enough
            }
        },
        'mask': '99 99',
        'mask_option': {placeholder: "X", autoclear: false},
    },
    'passport_num': {
        'rules': {
            regex: only_num,
            messages: {
                regex: message.not_enough
            }
        },
        'mask': '999999',
        'mask_option': {placeholder: "X", autoclear: false},
    },
    'passport_code': {
        'rules': {
            regex: nums_defis,
            messages: {
                regex: message.not_enough
            }
        },
        'mask': '999-999',
        'mask_option': {placeholder: "X", autoclear: false},
    },
    'passport_date': {
        'rules': {
            check_date_of_passport: true
        }
    },
    'cyrillic': {
        'rules': {
            regex: cyrillic,
            messages: {
                regex: message.cyrillic
            }
        }
    },
    'cyrillic_defis_apostrof': {
        'rules': {
            regex: cyrillic_apostrof_defis,
            messages: {
                regex: message.cyrillic_apostrof_defis
            }
        }
    },
    'house': {
        'rules': {
            regex: nums_letters_space_quote_defis,
            messages: {
                regex: message.nums_letters_space_quote_defis
            }
        }
    }

}


function validatorMethods() {
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || cyrillic_apostrof.test(value);
    }, message.lettersonly);


    $.validator.addMethod("check_date_of_birth", function (value, element) {
        if (this.optional(element)) {
            return true;
        }

        let container = $(element).closest('.form-input__date');
        let full_date = [];
        let can_continue = true;

        $(container.find('input[type=hidden]').get().reverse()).each(function() {
            let this_value = $(this).attr('data-value');
            full_date.push(this_value);
            if(typeof this_value === 'undefined')
                can_continue=false;
        });

        if(!can_continue){
            return true;
        }

        var mydate = new Date();
        mydate.setFullYear(full_date[0], full_date[1] - 1, full_date[2]);

        var minDate = new Date();
        minDate.setYear(minDate.getFullYear() - 18);

        // var maxDate = new Date();
        // maxDate.setYear(maxDate.getFullYear() - 66);

        if (minDate < mydate) {
            return false;
        }


        // if (maxDate > mydate) {
        //     setTimeout(function () {
        //         container.closest('.form-input').find('.form-input__error').html('<label class="errorText">'+message.max_age+'</label>')
        //     }, 200)
        //
        //     return false;
        // }


        return true;
    }, message.min_age);

    $.validator.addMethod("check_date_of_passport", function (value, element) {
        if (this.optional(element)) {
            return true;
        }

        let birth_container = $('input[data-validation="birth_date"]').closest('.form-input__date');
        let birth_date_arr = [];
        let can_continue = true;

        $(birth_container.find('input[type=hidden]').get().reverse()).each(function() {
            let this_value = $(this).attr('data-value');
            birth_date_arr.push(this_value);
            if(typeof this_value === 'undefined')
                can_continue=false;
        });
        if(!can_continue){
            return true;
        }
        let birth_date = new Date();
        birth_date.setFullYear(birth_date_arr[0], birth_date_arr[1] - 1, birth_date_arr[2]);
        birth_date.setYear(birth_date.getFullYear() + 14);




        let pass_container = $(element).closest('.form-input__date');
        let pass_date_arr = [];

        $(pass_container.find('input[type=hidden]').get().reverse()).each(function() {
            let this_value = $(this).attr('data-value');
            pass_date_arr.push(this_value);
            if(typeof this_value === 'undefined')
                can_continue=false;
        });
        if(!can_continue){
            return true;
        }
        let pass_date = new Date();
        pass_date.setFullYear(pass_date_arr[0], pass_date_arr[1] - 1, pass_date_arr[2]);


        if(birth_date >= pass_date) {
            return false;
        }

        return true;
    }, message.passport_date);


    $.validator.addMethod("regex", function (value, element, regexp) {
        var re = new RegExp(regexp);

        return value == '' || re.test(value);
    });


}

function inputsRules($form) {
    $('[required]', $form).each(function () {
        $(this).rules("add", {
            required: true,
            messages: {
                required: message.empty
            }
        });
    });

    $('[maxlength]', $form).each(function () {
        let max = $(this).attr('maxlength');
        $(this).rules("add", {
            maxlength: max,
            messages: {
                maxlength: message.max_length + max
            }
        });
    });

    $('[minlength]', $form).each(function () {
        let min = $(this).attr('minlength');
        $(this).rules("add", {
            minlength: min,
            messages: {
                minlength: message.min_length + min
            }
        });
    });

    if ($('[data-validation=tel]', $form).length) {
        $('[data-validation=tel]', $form).inputmask(mobile_mask, mobile_mask_option);
        console.log($('[data-validation=tel]', $form))
        $('[data-validation=tel]', $form).rules("add", {
            regex: mobile_regex,
            messages: {
                regex: message.not_enough,
                required: message.empty
            }
        });
    }

    $.each(inputs, function (data_id, opts) {
        var $input = $('[data-validation='+data_id+']', $form);

        if($input.length) {
            if (typeof opts.rules !== 'undefined') {
                validationRule($input,'add', opts.rules)
            }
            if (typeof opts.mask !== 'undefined' && typeof opts.mask_option !== 'undefined'){
                $input.inputmask(opts.mask, opts.mask_option)
            } else if(typeof opts.mask !== 'undefined'){
                $input.inputmask(opts.mask)
            }
        }
    });
}

function validate(form, options) {
    var setings = {
        errorFunction: null,
        submitFunction: null,
        highlightFunction: null,
        unhighlightFunction: null
    };

    validatorMethods();

    $.extend(setings, options);

    var $form = $(form);

    if ($form.length && $form.attr('novalidate') === undefined) {
        $form.on('submit', function (e) {
            e.preventDefault();
        });

        $form.validate({
            errorClass: 'errorText',
            focusCleanup: false,
            onclick: false,
            onfocusout: false,
            // onkeyup: false,
            focusInvalid: true,
            invalidHandler: function (event, validator) {
                if (typeof setings.errorFunction === 'function') {
                    setings.errorFunction(form);
                }
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.closest('.form-input').find('.form-input__error'));
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('error');
                $(element).closest('.form-input').addClass('error');
                if (typeof setings.highlightFunction === 'function') {
                    setings.highlightFunction(form);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('error');
                $(element).closest('.form-input').removeClass('error');
                if (typeof setings.unhighlightFunction === 'function') {
                    setings.unhighlightFunction(form);
                }
            },
            submitHandler: function (form) {
                $('[type=submit]').each(function (){
                    $(this).attr('disabled', 'disabled');
                });
                if (typeof setings.submitFunction === 'function') {
                    setings.submitFunction(form);
                } else {
                    $form[0].submit();
                }
            }
        });

        inputsRules($form);
    }
}

function validationRule(input, action, rule){
    input.each(function () {
        if(rule == 'required' && action == 'add') {
            $(this).rules('add', {
                required: true,
                messages: {
                    required: message.empty
                }
            });
        } else if(rule == 'required') {
            $(this).removeAttr('required')
            $(this).rules(action, rule);
        } else {
            $(this).rules(action, rule);
        }
    })
}



function inputDropdown() {
    let dropdowns = $('.form-input__dropdown');
    let input_container = dropdowns.parent();
    let input = input_container.find('input[type=text]');
    let dropdown = input_container.find('.form-input__dropdown__container');
    let dropdown_item = dropdown.find('li');
    let label = dropdowns.closest('.form-input').find('label')

    input.on('click', function () {
        let this_container = $(this).closest('.form-input__dropdown');
        let this_dropdown = this_container.find('.form-input__dropdown__container');


        dropdown.not(this_dropdown).slideUp(100);
        dropdowns.not(this.closest('.form-input__dropdown')).removeClass('open');

        this_dropdown.slideToggle(300);
        this_container.toggleClass('open');


        if(dropdowns.hasClass('open')) {
            label.addClass('open')
        } else {
            label.removeClass('open')
        }
    });

    dropdown.on('click', function () {
        $(this).slideUp(300);
        dropdowns.removeClass('open');
        label.removeClass('open');
    });

    inputDropdownListEvents($('.form-input__dropdown'));


    $(document).on('click', function (e) {
        let target = $(e.target);

        if(!target.closest('.form-input__date__input').length){
            dropdown.slideUp(300);
            dropdowns.removeClass('open');
            label.removeClass('open');
        }
    });
}

function inputDropdownListEvents(dropdowns) {
    let input_container = dropdowns.parent();
    let dropdown = input_container.find('.form-input__dropdown__container');
    let dropdown_item = dropdown.find('li');

    dropdown_item.on('click', function () {
        let this_item = $(this);
        let this_text = this_item.text();
        let this_container = this_item.closest('.form-input__dropdown').parent();
        let this_main_input = this_container.find('input[type=text]');
        let this_hidden_input = this_container.find('input[type=hidden]');
        let data_value = this_item.data('value');
        let all_items = this_container.find('li')

        all_items.removeClass('selected')
        this_item.addClass('selected');

        console.log('dat - value: '+data_value)
        if(data_value){
            this_main_input.attr('data-value', data_value)
            this_hidden_input.attr('data-value', data_value)
        }
        this_main_input.val(this_text)
        this_hidden_input.val(this_text)
        this_hidden_input.trigger('change')
        this_hidden_input.valid()
    });
}

function inputDate(){
    let dates = $('.form-input__date');

    function reloadDropdowns(this_date) {
        let this_container = this_date;
        let input_containers = this_container.find('.form-input__date__input');
        let this_days = this_container.find('.day_input');
        let days_list = this_days.closest('.form-input__dropdown').find('.form-input__dropdown__list');
        let this_months = this_container.find('.month_input');
        let months_list = this_months.closest('.form-input__dropdown').find('.form-input__dropdown__list');
        let this_years = this_container.find('.year_input');
        let year_list = this_years.closest('.form-input__dropdown').find('.form-input__dropdown__list');

        let num_month = parseInt(this_months.closest('.form-input__date__input').find('input[type=hidden]').attr('data-value'));

        let today = new Date();
        let curr_year = today.getFullYear();

        let max_days = 31;
        let min_year = 1920;

        if (num_month && this_years.val()) {
            max_days = new Date(parseInt(this_years.val()), num_month, 0).getDate();
            console.log('change max days to: ' + max_days)
        }

        // if(){
        //     curr_year = 2000
        // }

        days_list.empty();
        for (let days = 1; days <= max_days; days++) {
            days_list.append('<li data-value="' + days + '">' + days + '</li>')
        }

        months_list.empty();
        $.each(months_array, function (index, month) {
            months_list.append('<li data-value="' + (index + 1) + '">' + month + '</li>')
        })

        year_list.empty();
        for (min_year; min_year <= curr_year; min_year++) {
            year_list.append('<li data-value="' + min_year + '">' + min_year + '</li>')
        }

        input_containers.each(function () {
            let hidden_inp = $(this).find('input[type=hidden]');
            let dropd_inp = $(this).find('.form-input__dropdown>input');
            let selected = hidden_inp.attr('data-value');

            if(selected) {
                let item = $(this).find('li[data-value=' + selected + ']')

                if (item.length) {
                    item.addClass('selected')
                } else {
                    hidden_inp.val('')
                    hidden_inp.attr('data-value', '')
                    dropd_inp.val('')
                    dropd_inp.attr('data-value', '')
                }
            }
        });

    }

    dates.each(function () {
        reloadDropdowns($(this))

        $(this).find('input[type=hidden]').on('change', function () {
            reloadDropdowns($(this).closest('.form-input__date'));
            inputDropdownListEvents($(this).closest('.form-input__date'))
        })
    })
    inputDropdownListEvents($('.form-input__dropdown'))
}

$(document).ready(function() {
    inputDropdown();
    inputDate();

    validate('#form_1')
});
